package PackageMaven;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import commonmethods.CommonMethods;
import commonmethods.ObjectRepository;

public class RadioButtonsDemo extends CommonMethods implements ObjectRepository{

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\ghassani\\Downloads\\chromedriver_win32\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.seleniumeasy.com/test");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//a[text()='No, thanks!']")).click();
		
	
		ocm.NavigateToPage(driver, oInputForm,oRadioButtonDemo);
		
		ocm.fnClick(driver, oMaleRadioButton,oXtra);
		
		ocm.fnClick(driver,oGetCheckedValueButton,oXtra);
		
		String radioMessageSuccessMessage = driver.findElement(oWebElementRadioButtonMessage).getText();
		String radioButtonClickedValue = driver.findElement(oMaleRadioButton).getAttribute("value");
		if (radioMessageSuccessMessage.contains(radioButtonClickedValue)) {
			System.out.println("Correct message displayed :"+radioMessageSuccessMessage+":"+"Selected Checkbox :"+radioButtonClickedValue);
		}
		
		// Testing group radio sections
		
		ocm.fnClick(driver,oGroupSectionFemaleRadioButton,oXtra);
		ocm.fnClick(driver,oGroupSectionAgeGroupThird,oXtra);
		ocm.fnClick(driver,oGroupSectionGetValuesButton, oXtra);
		if (!driver.findElement(oGroupSectionPrintedValues).isDisplayed()) {
			System.out.println("Age group and Gender not displayed");
		}
		else System.out.println("Printed values are :\n"+driver.findElement(oGroupSectionPrintedValues).getText());
		
		driver.close();
		

	}

}
