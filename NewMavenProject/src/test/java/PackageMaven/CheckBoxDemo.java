package PackageMaven;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import commonmethods.ObjectRepository;

public class CheckBoxDemo implements commonmethods.CheckBoxDemo,ObjectRepository {
	
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\ghassani\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.seleniumeasy.com/test/");
		
		driver.manage().window().maximize();
		
		//Nvaigate to check box demo page
		ocm.NavigateToPage(driver, oInputForm, oSimpleForm);
		
		ocm.SumTwoNumbers(driver,"300","400");
		
		ocm.NavigateToPage(driver, oInputForm,oLinkCheckboxDemo);
		
		ocm.SingleCheckBoxDemo(driver, oCheckBox, oSuccessCheckboxSelectedMessage);
		
		ocm.MultiCheckBoxDemo(driver, oFirstCheckBox, oButtonCheckAll, oThirdCheckBox);
		
		driver.quit();
	}

}
