package PackageMaven;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonmethods.ObjectRepository;

public class AjexForm implements ObjectRepository {

//	public  void main(String[] args) {
	public void test() {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\ghassani\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.seleniumeasy.com/test");
		driver.manage().window().maximize();
		driver.findElement(noThanks).click();
		WebDriverWait wait = new WebDriverWait(driver,2);
		
		ocm.NavigateToPage(driver,oInputForm,oAjexFormPageOption);
		
		if (driver.findElement(oAjexFormBlock).isDisplayed()) {
			
			driver.findElement(oAjexFormNameField).sendKeys("Gulshan Hassani");
			driver.findElement(oAjexFormCommentField).sendKeys("Testing Ajex form with Selenium");
			driver.findElement(oAjexFormButton).click();
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(oAjexFormSubmittedSuccessfully));
			if (!driver.findElement(oAjexFormSubmittedSuccessfully).isDisplayed()) {
				System.out.println("Form did not submitted successfully");
			}
			else System.out.println("Form Submitted successfully");
			
		}
		
	}

}
