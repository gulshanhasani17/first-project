package commonmethods;


import org.openqa.selenium.By;


public interface CheckBoxDemo {


	By oLinkCheckboxDemo =  By.xpath("(//a[@href='./basic-checkbox-demo.html' and text()='Checkbox Demo'])[1]");
	By oButtonCheckAll = By.xpath(" //input[@value='Check All']");
	By oButtonUncheckAll = By.xpath(" //input[@value='Uncheck All']");
	By oFirstCheckBox = By.xpath("( //input[@class='cb1-element'])[1]");
	By oThirdCheckBox = By.xpath("( //input[@class='cb1-element'])[3]");
	By oCheckBox = By.xpath("//input[@id='isAgeSelected']");
	By oSuccessCheckboxSelectedMessage = By.xpath("//div[text()='Success - Check box is checked']");
	
	
}

