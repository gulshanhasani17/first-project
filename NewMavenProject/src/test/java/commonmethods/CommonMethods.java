package commonmethods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonMethods implements ObjectRepository,CheckBoxDemo {


	public void fnClick(WebDriver driver, By xpath, Object oXtra) {
		driver.findElement(xpath).click();
	}

	public void SumTwoNumbers(WebDriver driver,String n1, String n2) {
		driver.findElement(number1).sendKeys(n1);
		driver.findElement(number2).sendKeys(n2);
		fnClick(driver,oButtonSum,oXtra);
		ObjectExistenceMethod(driver,2);
		if (Integer.valueOf(driver.findElement(oSumDisplay).getText()) != Integer.valueOf(n1)+Integer.valueOf(n2)) {
			System.out.println("Incorrect sum displayed");
		}
		else {
			System.out.println("Correct Sum displayed :"+driver.findElement(oSumDisplay).getText());
		}

	}

	public boolean ObjectExistenceMethod(WebDriver driver,int time) {
		try{
			WebDriverWait wait = new WebDriverWait(driver,time);
			wait.until(ExpectedConditions.visibilityOfElementLocated(oSumLabel));
			return true;
		}
		catch (Exception strError){
			strError.getMessage();
			System.out.println("Label Sum A+B is missing from the page");
			return false;
		}


	}

	public void NavigateToPage(WebDriver driver,By DropDownName,By LinkName) {
		fnClick(driver,DropDownName,oXtra);
		fnClick(driver, LinkName, oXtra);
	}
	
	
	public void SingleCheckBoxDemo(WebDriver driver, By checkbox, By validation) {
		// WebDriverWait wait = new WebDriverWait(driver,2);
		if (!driver.findElement(validation).isDisplayed()) {
			driver.findElement(checkbox).click();
			if (driver.findElements(validation).isEmpty()) {
				System.out.println("Checkbox not checked or validation of page is failing");
			}
			else {
				System.out.println("Successfully validated the checkbox");
			}
		
			
		}
		else {
			System.out.println("Message is already displayed.Nothing to verify");
		}
		
	}
	


	public void MultiCheckBoxDemo(WebDriver driver,By checkbox, By buttonValue, By xTraElement) {
		fnClick(driver, checkbox, oXtra);
		fnClick(driver, xTraElement, oXtra);
		if (!driver.findElement(buttonValue).isDisplayed()) {
			System.out.println("Incorrect button is displayed :"+driver.findElement(buttonValue).getAttribute("value"));
		}
		else System.out.println("Correct button is displayed :"+driver.findElement(buttonValue).getAttribute("value")); 
		
		fnClick(driver, oButtonCheckAll, oXtra);
		if (!driver.findElement(oButtonUncheckAll).isDisplayed()) {
			System.out.println("Button value did not changed");
		}
		
		}
	
		
	}

