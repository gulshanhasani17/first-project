package commonmethods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CB_POM {

	WebDriver driver;
	
	By oLinkCheckboxDemo =  By.xpath("(//a[@href='./basic-checkbox-demo.html' and text()='Checkbox Demo'])[1]");
	By oButtonCheckAll = By.xpath(" //input[@value='Check All']");
	By oButtonUncheckAll = By.xpath(" //input[@value='Uncheck All']");
	By oFirstCheckBox = By.xpath("( //input[@class='cb1-element'])[1]");
	By oThirdCheckBox = By.xpath("( //input[@class='cb1-element'])[3]");
	By oCheckBox = By.xpath("//input[@id='isAgeSelected']");
	By oSuccessCheckboxSelectedMessage = By.xpath("//div[text()='Success - Check box is checked']");
	
	
	@FindBy(how=How.XPATH,using="")
	WebElement ele;
	
	
	WebElement ele1 = driver.findElement(oLinkCheckboxDemo);
	
	
	public void clickOnCheckBox() {
		ele.click();
	}
	
	
	public void clickonMultipleCheckBox() {
		ele.click();
		ele.click();
	}
	
	public void getPageTitle() {
		
		
	}
}
