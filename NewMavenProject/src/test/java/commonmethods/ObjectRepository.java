package commonmethods;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

public interface ObjectRepository {
	CommonMethods ocm = new CommonMethods();
	By oInputForm = By.xpath("//a[@class='dropdown-toggle' and contains(text(),'Input Forms')]");
	By noThanks = By.xpath("//a[text()='No, thanks!']");
	
	// Simple form
	By oSimpleForm = By.xpath("(//a[@href='./basic-first-form-demo.html' and text()='Simple Form Demo'])[1]");
	 By oEnterMessageField = By.xpath("//input[@placeholder='Please enter your Message']"); 
	 String str = "Hassanig";
	 By oStringValidation = By.xpath("//span[text()='"+str+"']");
	 By oShowMessage = By.xpath("//button[@onclick='showInput();' and text()='Show Message']");
	 By oValidateLabel = By.xpath("//label[contains(text(),'Your Message')]");
	 By number1 = By.xpath("//input[@placeholder='Enter value' and @id='sum1']");
	 By number2 = By.xpath("//input[@placeholder='Enter value' and @id='sum2']");
	 By oButtonSum = By.xpath("//button[@onclick='return total()']");
	 By oSumLabel = By.xpath("//label[text()=' Total a + b = ']");
	 Object oXtra = null;
	 By oSumDisplay = By.xpath("//span[@id='displayvalue']");
	// Simple form
	 // Radio buttons
	 By oRadioButtonDemo = By.xpath("(//a[text() = 'Radio Buttons Demo'])[1]");
	 By oMaleRadioButton = By.xpath("(//input[@type ='radio' and @value='Male' ])[1]");
	 By oGetCheckedValueButton = By.xpath("//button[@id='buttoncheck' and text()='Get Checked value' ]");
	 By oWebElementRadioButtonMessage = By.xpath("(//label[@class='radio-inline'])[2]/following-sibling::p[2]");
	 By oGroupSectionFemaleRadioButton = By.xpath("(//input[@value='Female'])[2]");
	 By oGroupSectionAgeGroupThird = By.xpath("//input[@value='15 - 50']");
	 By oGroupSectionGetValuesButton = By.xpath("//button[text()='Get values']");
	 By oGroupSectionPrintedValues = By.xpath("//p[@class='groupradiobutton']");
	// Radio buttons
	 // Dropdowns
	 By oDropdownJquerySelectDropDown = By.xpath("(//a[text()='JQuery Select dropdown'])[1]");
	 By oDropDownWithDisabledValues = By.xpath("//span[@class='select2-selection__rendered' and @title='American Samoa']");
	 By oPuertoRico = By.xpath("//li[text()='Puerto Rico']");
	// Drop downs
	 // Ajex form page
	 By oAjexFormPageOption = By.xpath("(//a[text()='Ajax Form Submit'])[1]");
	 By oAjexFormBlock = By.xpath("//div[@class='panel-heading' and text()='Ajax Form']");
	 By oAjexFormNameField = By.xpath("//input[@name='title']");
	 By oAjexFormCommentField = By.xpath("//textarea[@name='description']");
	 By oAjexFormButton = By.xpath("//input[@type='button']");
	 By oAjexFormSubmittedSuccessfully = By.xpath("//div[@id='submit-control']");
	 // Ajex Form
	 
	 // Input Form
	 By oInputFormPage = By.xpath("(//a[text()='Input Form Submit'])[1]");
	 By oInputFormHeading = By.xpath("//h2[text()='Input form with validations']");
	 By oFirstName = By.xpath("//input[@placeholder='First Name']");
	 By oLastName = By.xpath("//input[@placeholder='Last Name']");
	 By oEmailAddress = By.xpath("//input[@placeholder='E-Mail Address']");
	 By oPhone = By.xpath("//input[@name='phone']");
	 By oAddress = By.xpath("//input[@placeholder='Address']");
	 By oCityName = By.xpath("//input[@placeholder='city']");
	 By oDropDownState = By.xpath("//select[@name='state']");
	 By oZipCode = By.xpath("//input[@placeholder='Zip Code']");
	 By oDomainName = By.xpath("//input[@placeholder='Website or domain name']");
	 By oRadioYes = By.xpath("//input[@type='radio' and @value='yes']");
	 By oRadioNo = By.xpath("//input[@type='radio' and @value='no']");
	 By oProductDescription = By.xpath("//textarea[@placeholder='Project Description']");
	 By oSubmitButton = By.xpath("//button[@type='submit']");
	 
}
